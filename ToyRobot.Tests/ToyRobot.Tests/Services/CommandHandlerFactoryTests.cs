﻿using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using ToyRobot.Services;
using ToyRobot.Services.CommandHandlerServices;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Tests.Services
{
    [TestFixture]
    public class CommandHandlerFactoryTests
    {
        private IEnumerable<ICommandHandlerService> _subCommandHandlerServices;
        private ICommandValidatorService _subCommandValidatorService;

        [SetUp]
        public void SetUp()
        {
            _subCommandHandlerServices = Substitute.For<IEnumerable<ICommandHandlerService>>();
            _subCommandValidatorService = Substitute.For<ICommandValidatorService>();
        }

        private CommandHandlerFactory CreateFactory()
        {
            return new CommandHandlerFactory(
                _subCommandHandlerServices,
                _subCommandValidatorService);
        }

        private CommandHandlerFactory CreateFactory(IEnumerable<ICommandHandlerService> commandHandlerServices)
        {
            return new CommandHandlerFactory(commandHandlerServices, _subCommandValidatorService);
        }

        [Test]
        public void CreateCommandHandlerService_Should_Return_HandlerService_When_Command_Valid()
        {
            // Arrange
            var subRobotService = Substitute.For<IRobotDriverService>();
            var moveCommandHandlerServiceToReturn = new MoveCommandHandlerService(subRobotService);

            var factory = CreateFactory(new List<ICommandHandlerService>() { moveCommandHandlerServiceToReturn }); 

            const string command = "test_move_command";

            _subCommandValidatorService.IsMoveCommand(command).Returns(true);

            // Act
            var result = factory.CreateCommandHandlerService(command);

            // Assert
            Assert.IsAssignableFrom<MoveCommandHandlerService>(result); 
        }

        [Test]
        public void CreateCommandHandlerService_Should_Return_Null_When_Command_Invalid()
        {
            // Arrange
            var factory = CreateFactory();

            const string command = "test_move_command";

            // Act
            var result = factory.CreateCommandHandlerService(command);

            // Assert
            Assert.IsNull(result);
        }
    }
}
