﻿using NSubstitute;
using NUnit.Framework;
using ToyRobot.Services;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Tests.Services
{
    [TestFixture]
    public class CommandHandlerTests
    {
        private ICommandHandlerFactory _subCommandHandlerFactory;

        [SetUp]
        public void SetUp()
        {
            _subCommandHandlerFactory = Substitute.For<ICommandHandlerFactory>();
        }

        private CommandHandler CreateCommandHandler()
        {
            return new CommandHandler(_subCommandHandlerFactory);
        }

        [Test]
        public void Handle_Should_Call_Handler_When_Factory_Returns_Command_Handler()
        {
            // Arrange
            var commandHandler = CreateCommandHandler();

            string command = "test_command";

            var returnedCommandHandler = Substitute.For<ICommandHandlerService>();
            _subCommandHandlerFactory.CreateCommandHandlerService(command).Returns(returnedCommandHandler);

            // Act
            commandHandler.Handle(command);

            // Assert
            returnedCommandHandler.Received(1).HandleCommand(command);
        }
    }
}
