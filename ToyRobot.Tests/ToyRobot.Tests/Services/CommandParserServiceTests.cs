﻿using NUnit.Framework;
using ToyRobot.Models;
using ToyRobot.Services;

namespace ToyRobot.Tests.Services
{
    [TestFixture]
    public class CommandParserServiceTests
    {
        private static CommandParserService CreateService()
        {
            return new CommandParserService();
        }

        [TestCase("3,3,NORTH", Direction.NORTH)]
        [TestCase("3, 3, NORTH", Direction.NORTH)]
        [TestCase("3,3,EAST", Direction.EAST)]
        [TestCase("3,3,Invalid", null)]
        [TestCase("3,3,", null)]
        [TestCase("3,3,LEFT", null)]
        [TestCase("3,3,WEST,1235", null)]
        [TestCase("", null)]
        public void ParseInputDirection_Should_Parse_Direction_When_Input_Valid(string input, Direction? expectedResult)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ParseInputDirection(input);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("2,3,EAST", 2, 3)]
        [TestCase("0, 0, NORTH", 0, 0)]
        [TestCase("0, 0 ", 0, 0)]
        [TestCase("MOVE", null, null)]
        [TestCase("2,EAST,4", null, null)]
        [TestCase("", null, null)]
        public void ParseInputPosition_Should_Parse_Position_When_Input_Valid(string input, int? expectedX, int? expectedY)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ParseInputPosition(input);

            // Assert
            Assert.AreEqual(expectedX, result?.Item1 ?? null);
            Assert.AreEqual(expectedY, result?.Item2 ?? null);
        }

        [TestCase("LEFT", Rotation.LEFT)]
        [TestCase("RIGHT", Rotation.RIGHT)]
        [TestCase("LEFT ", Rotation.LEFT)]
        [TestCase("RIGHT ", Rotation.RIGHT)]
        [TestCase("", null)]
        [TestCase("1,2,LEFT", null)]
        public void ParseInputRotation_StateUnderTest_ExpectedBehavior(string input, Rotation? expectedRotation)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.ParseInputRotation(input);

            // Assert
            Assert.AreEqual(expectedRotation, result);
        }
    }
}
