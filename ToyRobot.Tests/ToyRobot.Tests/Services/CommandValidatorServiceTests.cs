﻿using NUnit.Framework;
using ToyRobot.Services;

namespace ToyRobot.Tests.Services
{
    [TestFixture]
    public class CommandValidatorServiceTests
    {
        private static CommandValidatorService CreateService()
        {
            return new CommandValidatorService();
        }

        [TestCase("MOVE", true)]
        [TestCase("MOVE ", true)]
        [TestCase(" MOVE", false)]
        [TestCase("move", false)]
        [TestCase("PLACE", false)]
        [TestCase("", false)]
        public void IsMoveCommand_Should_Return_True_For_Valid_Move_Command(string input, bool expectedResult)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.IsMoveCommand(input);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("PLACE ", true)]
        [TestCase("PLACE", false)]
        [TestCase("place ", false)]
        [TestCase("LEFT", false)]
        [TestCase("", false)]
        public void IsPlaceCommand_Should_Return_True_For_Valid_Place_Command(string input, bool expectedResult)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.IsPlaceCommand(input);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase("REPORT", true)]
        [TestCase("REPORT ", true)]
        [TestCase(" REPORT", false)]
        [TestCase("report ", false)]
        [TestCase("PLACE", false)]
        [TestCase("", false)]
        public void IsReportCommand_Should_Return_True_For_Valid_Report_Command(string input, bool expectedResult)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.IsReportCommand(input);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }


        [TestCase("LEFT", true)]
        [TestCase("RIGHT", true)]
        [TestCase("LEFT ", true)]
        [TestCase("RIGHT ", true)]
        [TestCase(" LEFT", false)]
        [TestCase(" RIGHT", false)]
        [TestCase("left", false)]
        [TestCase("right", false)]
        [TestCase("MOVE", false)]
        [TestCase("", false)]
        public void IsRotateCommand_Should_Return_True_For_Valid_Rotate_Command(string input, bool expectedResult)
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.IsRotateCommand(input);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
