﻿using NUnit.Framework;
using System;
using ToyRobot.Models;
using ToyRobot.Services;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Tests.Services
{
    [TestFixture]
    public class RobotDriverServiceTests
    {
        private static IRobotDriverService CreateService()
        {
            return new RobotDriverService();
        }

        [Test]
        public void PlaceRobot_Should_Place_Robot_When_Robot_Not_Running()
        {
            // Arrange
            var service = CreateService();

            const int x = 4;
            const int y = 0;

            Direction? direction = Direction.WEST;

            // Act
            service.PlaceRobot(x, y, direction);

            // Assert
            Assert.AreEqual(x, service.GetRobotPositon().Item1);
            Assert.AreEqual(y, service.GetRobotPositon().Item2);
        }

        [Test]
        public void PlaceRobot_Should_Not_Place_Robot_When_Robot_Not_Running_Without_Direction()
        {
            // Arrange
            var service = CreateService();

            const int x = 4;
            const int y = 0;

            Direction? direction = null;

            // Act
            service.PlaceRobot(x, y, direction);

            // Assert
            Assert.IsFalse(service.IsRobotRunning);
        }

        [Test]
        public void PlaceRobot_Should_Not_Place_Robot_When_Coordinate_Not_Valid()
        {
            // Arrange
            var service = CreateService();

            const int x = 6;
            const int y = 0;

            Direction? direction = Direction.WEST;

            // Act
            service.PlaceRobot(x, y, direction);

            // Assert
            Assert.Throws<NullReferenceException>(() => service.GetRobotPositon());
        }

        [Test]
        public void PlaceRobot_Should_Place_Robot_Without_Changing_Direction_When_Robot_Running()
        {
            // Arrange
            var service = CreateService();

            const int initalXPosition = 1;
            const int initalYPositiony = 2;

            var initialDirection = Direction.WEST;

            PlaceRobot(service, initalXPosition, initalYPositiony, initialDirection);

            const int x = 4;
            const int y = 5;

            // Act
            service.PlaceRobot(x, y, null);

            // Assert
            Assert.AreEqual(x, service.GetRobotPositon().Item1);
            Assert.AreEqual(y, service.GetRobotPositon().Item2);
        }

        [Test]
        public void MoveRobot_Should_Not_Move_When_Robot_Not_Running()
        {
            // Arrange
            var service = CreateService();

            // Act
            service.MoveRobot();

            // Assert
            Assert.Throws<NullReferenceException>(() => service.GetRobotPositon());
        }

        [Theory]
        [TestCase(Direction.EAST, 5, 5)]
        [TestCase(Direction.NORTH, 5, 5)]
        [TestCase(Direction.WEST, 0, 0)]
        [TestCase(Direction.SOUTH, 0, 0)]
        public void MoveRobot_Should_Not_Move_When_Robot_On_The_Edge(Direction direction, int x, int y)
        {
            // Arrange
            var service = CreateService();

            PlaceRobot(service, x, y, direction);

            // Act
            service.MoveRobot();

            // Assert
            Assert.AreEqual(x, service.GetRobotPositon().Item1);
            Assert.AreEqual(y, service.GetRobotPositon().Item2);
        }

        [TestCase(Direction.NORTH, Direction.EAST, Rotation.RIGHT)]
        [TestCase(Direction.EAST, Direction.SOUTH, Rotation.RIGHT)]
        [TestCase(Direction.SOUTH, Direction.WEST, Rotation.RIGHT)]
        [TestCase(Direction.WEST, Direction.NORTH, Rotation.RIGHT)]
        [TestCase(Direction.NORTH, Direction.WEST, Rotation.LEFT)]
        [TestCase(Direction.WEST, Direction.SOUTH, Rotation.LEFT)]
        [TestCase(Direction.SOUTH, Direction.EAST, Rotation.LEFT)]
        [TestCase(Direction.EAST, Direction.NORTH, Rotation.LEFT)]
        public void RotateRobot_Should_Rotate_Robot_90_Degrees(Direction initialDirection, Direction rotatedDirection, Rotation rotation)
        {
            // Arrange
            var service = CreateService();

            PlaceRobot(service, 3, 3, initialDirection);

            // Act
            service.RotateRobot(rotation);

            // Assert
            Assert.AreEqual(service.GetRobotDirection(), rotatedDirection);
        }
     
        [Test]
        public void GenerateRobot_Should_Return_Report_When_Robot_Running()
        {
            // Arrange
            var service = CreateService();

            PlaceRobot(service, 3, 3, Direction.NORTH);

            // Act
            var result = service.GenerateReport();

            // Assert
            Assert.AreEqual(result, "Output: 3,3,NORTH");
        }

        [Test]
        public void GenerateRobot_Should_Not_Return_Report_When_Robot_Not_Running()
        {
            // Arrange
            var service = CreateService();

            // Act
            var result = service.GenerateReport();

            // Assert
            Assert.IsNull(result);
        }

        private static void PlaceRobot(IRobotDriverService service, int x, int y, Direction direction)
        {
            service.PlaceRobot(x, y, direction);
        }
    }
}
