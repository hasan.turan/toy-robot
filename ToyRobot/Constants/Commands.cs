﻿namespace ToyRobot.Constants
{
    public class Commands
    {
        public const string PlaceCommand = "PLACE ";
        public const string ReportCommand = "REPORT";
        public const string MoveCommand = "MOVE";
        public const string RotateLeftCommand = "LEFT";
        public const string RotateRightCommand = "RIGHT";
    }
}
