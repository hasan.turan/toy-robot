﻿using ToyRobot.Models;

namespace ToyRobot.Helpers
{
    public static class RotationHelpers
    {
        public static Direction GetRotatedDirection(Rotation rotation, Direction currentDirection)
        {
            if (rotation.Equals(Rotation.LEFT))
            {
                switch (currentDirection)
                {
                    case Direction.NORTH:
                        return Direction.WEST;
                    case Direction.SOUTH:
                        return Direction.EAST;
                    case Direction.WEST:
                        return Direction.SOUTH;
                    case Direction.EAST:
                        return Direction.NORTH;
                }
            }

            if (rotation.Equals(Rotation.RIGHT))
            {
                switch (currentDirection)
                {
                    case Direction.NORTH:
                        return Direction.EAST;
                    case Direction.SOUTH:
                        return Direction.WEST;
                    case Direction.WEST:
                        return Direction.NORTH;
                    case Direction.EAST:
                        return Direction.SOUTH;
                }
            }

            return currentDirection;
        }
    }
}
