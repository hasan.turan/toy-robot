﻿namespace ToyRobot.Models
{
    public record Position
    {
        public int X;
        public int Y;
    }
}
