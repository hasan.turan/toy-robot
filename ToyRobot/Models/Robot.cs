﻿namespace ToyRobot.Models
{
    public class Robot
    {
        public Direction Direction { get; set; }
        public Position Position { get; set; }
        public bool IsPlaced { get; set; }
    }
}
