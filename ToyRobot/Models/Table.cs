﻿using System;

namespace ToyRobot.Models
{
    public class Table
    {
        private const int TableSize = 6;

        public static bool IsValidPosition(int x, int y)
        {
            if (x >= TableSize || y >= TableSize || x < 0 || y < 0)
            {
                return false;
            }

            return true;
        }

        public static Tuple<int, int> GetNextMovePosition(int x, int y, Direction direction)
        {
            switch (direction)
            {
                case Direction.EAST:
                    return Tuple.Create(x + 1, y);
                case Direction.WEST:
                    return Tuple.Create(x - 1, y);
                case Direction.NORTH:
                    return Tuple.Create(x, y + 1);
                case Direction.SOUTH:
                    return Tuple.Create(x, y - 1);
                default: return null;
            }
        }
    }
}
