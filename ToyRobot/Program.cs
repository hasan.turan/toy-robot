﻿using Microsoft.Extensions.DependencyInjection;
using System;
using ToyRobot.Services;
using ToyRobot.Services.CommandHandlerServices;
using ToyRobot.Services.Interfaces;

namespace ToyRobot
{
    internal class Program
    {       
        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            ConfigureServices(services);

            using var serviceScope = services.BuildServiceProvider().CreateScope();
            var serviceProvider = serviceScope.ServiceProvider;

            var commandTranslatorService = serviceProvider.GetRequiredService<ICommandHandler>();

            while (true)
            {
                var command = Console.ReadLine();
                if (!string.IsNullOrEmpty(command))
                {
                    commandTranslatorService.Handle(command);
                }
            }
        }

        // Register DI services
        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IRobotDriverService, RobotDriverService>();
            services.AddSingleton<ICommandHandler, CommandHandler>();

            services.AddScoped<ICommandValidatorService, CommandValidatorService>();
            services.AddScoped<ICommandParserService, CommandParserService>();
            services.AddScoped<ICommandHandlerFactory, CommandHandlerFactory>();

            services.AddScoped<ICommandHandlerService, PlaceCommandHandlerService>();
            services.AddScoped<ICommandHandlerService, ReportCommandHandlerService>();
            services.AddScoped<ICommandHandlerService, MoveCommandHandlerService>();
            services.AddScoped<ICommandHandlerService, RotateCommandHandlerService>();
        }
    }
}
