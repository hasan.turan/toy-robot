﻿using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services
{
    public class CommandHandler : ICommandHandler
    {
        private readonly ICommandHandlerFactory _handlerFactory;
        public CommandHandler(ICommandHandlerFactory handlerFactory)
        {
            _handlerFactory = handlerFactory;
        }

        public void Handle(string command)
        {
            var handler = _handlerFactory.CreateCommandHandlerService(command);
            if (handler is null)
            {
                return;
            }

            handler.HandleCommand(command);
        }        
    }
}
