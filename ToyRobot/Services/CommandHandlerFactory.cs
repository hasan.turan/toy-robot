﻿using System.Collections.Generic;
using System.Linq;
using ToyRobot.Services.CommandHandlerServices;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services
{
    public class CommandHandlerFactory : ICommandHandlerFactory
    {
        private readonly IEnumerable<ICommandHandlerService> _commandHandlerServices;
        private readonly ICommandValidatorService _commandValidatorService;
        public CommandHandlerFactory(IEnumerable<ICommandHandlerService> commandHandlerServices, ICommandValidatorService commandValidatorService)
        {
            _commandHandlerServices = commandHandlerServices;
            _commandValidatorService = commandValidatorService;
        }

        public ICommandHandlerService CreateCommandHandlerService(string command)
        {
            if (_commandValidatorService.IsPlaceCommand(command))
            {
                return _commandHandlerServices.First(x => x is PlaceCommandHandlerService);
            }

            if (_commandValidatorService.IsReportCommand(command))
            {
                return _commandHandlerServices.First(x => x is ReportCommandHandlerService);
            }

            if (_commandValidatorService.IsMoveCommand(command))
            {
                return _commandHandlerServices.First(x => x is MoveCommandHandlerService);
            }

            if (_commandValidatorService.IsRotateCommand(command))
            {
                return _commandHandlerServices.First(x => x is RotateCommandHandlerService);
            }

            return null;
        }
    }
}
