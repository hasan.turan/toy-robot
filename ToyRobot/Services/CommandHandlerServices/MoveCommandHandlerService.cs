﻿using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services.CommandHandlerServices
{
    public class MoveCommandHandlerService : ICommandHandlerService
    {
        private readonly IRobotDriverService _robotDriverService;
        public MoveCommandHandlerService(IRobotDriverService robotDriverService)
        {
            _robotDriverService = robotDriverService;
        }

        public void HandleCommand(string command)
        {
            _robotDriverService.MoveRobot();
        }
    }
}
