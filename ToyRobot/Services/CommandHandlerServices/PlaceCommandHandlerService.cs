﻿using ToyRobot.Constants;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services.CommandHandlerServices
{
    public class PlaceCommandHandlerService : ICommandHandlerService
    {
        private readonly IRobotDriverService _robotDriverService;
        private readonly ICommandParserService _commandParserService;
        public PlaceCommandHandlerService(IRobotDriverService robotDriverService, ICommandParserService commandParserService)
        {
            _commandParserService = commandParserService;
            _robotDriverService = robotDriverService;
        }

        public void HandleCommand(string command)
        {
            command = command.Replace(Commands.PlaceCommand, string.Empty);

            var coordinates = _commandParserService.ParseInputPosition(command);
            if (coordinates is null)
            {
                return;
            }

            var direction = _commandParserService.ParseInputDirection(command);

            _robotDriverService.PlaceRobot(coordinates.Item1, coordinates.Item2, direction);
        }
    }
}
