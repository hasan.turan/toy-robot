﻿using System;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services.CommandHandlerServices
{
    public class ReportCommandHandlerService : ICommandHandlerService
    {
        private readonly IRobotDriverService _robotDriverService;
        public ReportCommandHandlerService(IRobotDriverService robotDriverService)
        {
            _robotDriverService = robotDriverService;       
        }

        public void HandleCommand(string command)
        {
            var report = _robotDriverService.GenerateReport();
            if(string.IsNullOrEmpty(report))
            {
                return;
            }

            Console.WriteLine(report);
        }
    }
}
