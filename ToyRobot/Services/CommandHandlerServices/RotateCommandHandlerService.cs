﻿using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services.CommandHandlerServices
{
    public class RotateCommandHandlerService : ICommandHandlerService
    {
        private readonly IRobotDriverService _robotDriverService;
        private readonly ICommandParserService _commandParserService;
        public RotateCommandHandlerService(IRobotDriverService robotDriverService, ICommandParserService commandParserService)
        {
            _robotDriverService = robotDriverService;
            _commandParserService = commandParserService;
        }

        public void HandleCommand(string command)
        {
            var rotation = _commandParserService.ParseInputRotation(command);
            if (rotation is null || !rotation.HasValue)
            {
                return;
            }

            _robotDriverService.RotateRobot(rotation.Value);
        }
    }
}
