﻿using System;
using System.Linq;
using ToyRobot.Models;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services
{
    public class CommandParserService : ICommandParserService
    {
        public Direction? ParseInputDirection(string command)
        {
            var directionCommand = command.Split(',').Last().Trim();
            if (!Enum.GetNames(typeof(Direction)).ToList().Any(x => x.Equals(directionCommand, StringComparison.InvariantCulture)))
            {
                return null;
            }

            if (Enum.TryParse(typeof(Direction), directionCommand, out var result))
            {

                return (Direction)result;
            }

            return null;
        }

        public Tuple<int, int> ParseInputPosition(string command)
        {
            var coordinates = command.Split(',').Take(2);

            if (int.TryParse(coordinates.FirstOrDefault().Trim(), out int xCoordinateResult)
                && int.TryParse(coordinates.LastOrDefault().Trim(), out int yCoordinateResult))
            {
                return Tuple.Create(xCoordinateResult, yCoordinateResult);
            }

            return null;
        }

        public Rotation? ParseInputRotation(string command)
        {
            if (Enum.TryParse(typeof(Rotation), command.TrimEnd(), out var result))
            {
                return (Rotation)result;
            }

            return null;
        }
    }
}
