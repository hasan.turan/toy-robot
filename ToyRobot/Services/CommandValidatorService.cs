﻿using System;
using ToyRobot.Constants;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services
{
    public class CommandValidatorService : ICommandValidatorService
    {
        public bool IsMoveCommand(string command)
        {
            return command.TrimEnd().Equals(Commands.MoveCommand, StringComparison.InvariantCulture);
        }

        public bool IsPlaceCommand(string command)
        {
            return command.StartsWith(Commands.PlaceCommand);
        }

        public bool IsReportCommand(string command)
        {
            return command.TrimEnd().Equals(Commands.ReportCommand, StringComparison.InvariantCulture);
        }

        public bool IsRotateCommand(string command)
        {
            return command.TrimEnd().Equals(Commands.RotateLeftCommand, StringComparison.InvariantCulture) ||
                command.TrimEnd().Equals(Commands.RotateRightCommand, StringComparison.InvariantCulture);
        }
    }
}
