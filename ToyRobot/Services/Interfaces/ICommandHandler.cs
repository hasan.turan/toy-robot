﻿namespace ToyRobot.Services.Interfaces
{
    public interface ICommandHandler
    {
        void Handle(string command);
    }
}
