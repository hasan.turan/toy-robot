﻿namespace ToyRobot.Services.Interfaces
{
    public interface ICommandHandlerFactory
    {
        ICommandHandlerService CreateCommandHandlerService(string command);
    }
}
