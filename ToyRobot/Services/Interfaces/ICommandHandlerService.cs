﻿namespace ToyRobot.Services.Interfaces
{
    public interface ICommandHandlerService
    {
        void HandleCommand(string command);
    }
}
