﻿using System;
using ToyRobot.Models;

namespace ToyRobot.Services.Interfaces
{
    public interface ICommandParserService
    {
        Tuple<int, int> ParseInputPosition(string command);
        Direction? ParseInputDirection(string command);
        Rotation? ParseInputRotation(string command);
    }
}
