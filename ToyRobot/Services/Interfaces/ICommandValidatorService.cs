﻿namespace ToyRobot.Services.Interfaces
{
    public interface ICommandValidatorService
    {
        bool IsPlaceCommand(string command);
        bool IsReportCommand(string command);
        bool IsMoveCommand(string command);
        bool IsRotateCommand(string command);
    }
}
