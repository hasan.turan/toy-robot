﻿using System;
using ToyRobot.Models;

namespace ToyRobot.Services.Interfaces
{
    public interface IRobotDriverService
    {
        bool IsRobotRunning { get; }

        void PlaceRobot(int x, int y, Direction? direction);

        void MoveRobot();

        void RotateRobot(Rotation rotation);

        Tuple<int, int> GetRobotPositon();

        Direction GetRobotDirection();

        string GenerateReport();
    }
}
