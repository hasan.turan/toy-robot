﻿using ToyRobot.Models;
using System;
using ToyRobot.Helpers;
using ToyRobot.Services.Interfaces;

namespace ToyRobot.Services
{
    public class RobotDriverService : IRobotDriverService
    {
        private readonly Robot Robot = new();

        public bool IsRobotRunning => Robot.IsPlaced;

        public void PlaceRobot(int x, int y, Direction? direction)
        {
            if (!IsRobotRunning && direction is null)
            {
                return;
            }

            if (Table.IsValidPosition(x, y))
            {
                Robot.Position = new Position { X = x, Y = y };

                // if the robot is already running, should not change direction
                // as per solution requirement 8
                if (!IsRobotRunning && direction.HasValue)
                {
                    Robot.Direction = direction.Value;
                }

                Robot.IsPlaced = true;
            }

            return;
        }

        public void MoveRobot()
        {
            if (IsRobotRunning)
            {
                var nextMovePosition = Table.GetNextMovePosition(Robot.Position.X, Robot.Position.Y, Robot.Direction);
                if (nextMovePosition is null)
                {
                    return;
                }

                if (Table.IsValidPosition(nextMovePosition.Item1, nextMovePosition.Item2))
                {
                    Robot.Position = Robot.Position with { X = nextMovePosition.Item1, Y = nextMovePosition.Item2 };
                }
            }

            return;
        }

        public void RotateRobot(Rotation rotation)
        {
            if (IsRobotRunning)
            {
                var nextDirection = RotationHelpers.GetRotatedDirection(rotation, Robot.Direction);
                Robot.Direction = nextDirection;
            }

            return;
        }
       
        public Tuple<int, int> GetRobotPositon()
        {
            return Tuple.Create(Robot.Position.X, Robot.Position.Y);
        }

        public Direction GetRobotDirection()
        {
            return Robot.Direction;
        }

        public string GenerateReport()
        {
            if (!IsRobotRunning)
            {
                return null;
            }

            return $"Output: {Robot.Position.X},{Robot.Position.Y},{Robot.Direction}";
        }
    }
}
